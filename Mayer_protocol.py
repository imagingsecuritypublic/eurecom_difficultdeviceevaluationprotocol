import tensorflow.keras as keras 
import warnings, generator, dataset, argparse, os
import numpy as np
import pandas as pd
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.initializers import *
from sklearn import metrics
warnings.filterwarnings('ignore')

def train(model, name, train, val, epochs = 30):
    ## Callbacks ##
    MC = ModelCheckpoint(name, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    ES = EarlyStopping(monitor='val_acc', min_delta=0.01, patience = 2, verbose = 1, restore_best_weights=True)
    RL = ReduceLROnPlateau(monitor="val_acc", mode="max", factor=0.1, patience=2, verbose=1)
    CL =  ConstrainLayer()
    ## Train ##
    model.fit(train,
                        steps_per_epoch = train.__len__(),
                        epochs = epochs,
                        verbose = 1,
                        callbacks= [RL, MC, CL],
                        validation_data = val,
                        validation_steps = val.__len__())

def evaluation(model, test):
    acc=0
    auc=0
    ## Generator ##
    X, y = test.__getitem__(1)
    #pred = model.predict(X)
    #acc += metrics.accuracy_score(y, np.round(abs(pred)))
    acc += model.evaluate(X, y)[1]
    #auc += metrics.roc_auc_score(y, pred)
    return (acc*100), auc
    
def load_json_model(path, nb_classes=0):
    model = load_model(path)
    print("Loaded model from disk")
    if nb_classes != 0:
        x = model.layers[-2].output
        prediction = Dense(nb_classes, activation='softmax', name ='final')(x)
        model = Model(inputs=model.inputs, outputs=prediction)
        sgd = SGD(lr= 0.005, momentum = 0.95, decay=0.0005)
        model.compile(loss="binary_crossentropy", optimizer=sgd, metrics=['acc'])
    return model

def conv2d(x, w, k_size, stride=(1, 1), pad='SAME', k_ini="glorot_uniform", bias = True, b_ini='zeros'):
    return Conv2D(w, kernel_size = k_size, strides=stride, padding=pad, use_bias=bias, kernel_initializer=k_ini, bias_initializer=b_ini)(x)

def max_pooling(x, name, k_size=(3, 3), stride=(2, 2)):
    return MaxPooling2D(pool_size=k_size, strides=stride, padding='VALID',name=name)(x)

def fc_layer(x, w, name, act = 'tanh', k_ini="glorot_uniform", bias = True, b_ini="zeros"):
    return Dense(w, activation=act, use_bias=bias, kernel_initializer=k_ini, bias_initializer=b_ini, name=name)(x)

def batch_norm(x):
    return BatchNormalization(axis=-1, center=True,scale=True)(x)

#MISLNet 128x128 version
def MILSNet128(img_width, img_height, cam, nprefilt=3):
    inputs = Input(shape = (img_width, img_height,3))
    x = Conv2D(filters = nprefilt, kernel_size =  (5,5), padding = 'VALID', use_bias= True,kernel_initializer='glorot_uniform', bias_initializer= 'zeros')(inputs)
    # Block 1 #
    x = conv2d(x, 96, (7, 7), stride=(2, 2))
    x = batch_norm(x)
    x = Activation('tanh')(x)
    x = max_pooling(x, name='maxpool_conv1')
    # Block 2 #
    x = conv2d(x, 64, (5, 5))
    x = batch_norm(x)
    x = Activation('tanh')(x)
    x = max_pooling(x, name='maxpool_conv2')
    # Block 3 # 
    x = conv2d(x, 64, (5, 5))
    x = batch_norm(x)
    x = Activation('tanh')(x)
    x = max_pooling(x, name='maxpool_conv3')
    # Block 4 #
    x = conv2d(x, 128, (1, 1))
    x = batch_norm(x)
    x = Activation('tanh')(x)
    x = max_pooling(x, name='maxpool_conv4')
    # Classification #
    x = Flatten()(x)
    x = fc_layer(x, 200, name='dense1_out')
    x = fc_layer(x, 200, name='dense2_out')
    out = fc_layer(x, cam, act="softmax", name ="final")
    model = Model(inputs, out)
    sgd = SGD(lr= 0.001, momentum = 0.95, decay=0.0005)
    model.compile(optimizer = sgd, loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    return model

#Similarity Network (including feature extractors) 128x128 version
def Similarity_128(model_path, img_width, img_height, nprefilt=6,nb12=2048,nb3=64):

    left_input = Input(shape=(img_width, img_height,3))
    right_input = Input(shape=(img_width, img_height,3))

    model = load_model(model_path)
    output = model.layers[-2].output
    baseline = Model(model.inputs, output)

    MISL_output1 = baseline(left_input)
    MISL_output2 = baseline(right_input)

    fc1 = Dense(nb12)(MISL_output1)
    fcb1 = Dropout(0.5)(fc1)

    fc2 = Dense(nb12)(MISL_output2)
    fcb2 = Dropout(0.5)(fc2)

    fcb1b2 = Multiply()([fcb1,fcb2])
    x = Concatenate(axis=1)([fcb1,fcb1b2,fcb2])
    fc3 = Dense(nb3, activation= 'relu')(x)

    output = Dense(1, activation='sigmoid')(fc3)
    siamese = Model([left_input, right_input], output)
    sgd = SGD(lr= 0.005, momentum = 0.95, decay=0.0005)
    siamese.compile(loss="binary_crossentropy", optimizer=sgd, metrics=['acc'])
    siamese.summary()
    return siamese



def main():
    ## Variables ##
    path = "//medias/db/ImagingSecurity_misc/Berthet/Camera_identification/"
    ## Args ##
    parser = argparse.ArgumentParser(description='Chose the parameters for training or testing.')
    parser.add_argument("-m", "--mode", type=str, required=True,
                        help='Chose the mode you want: extractor - train - test')
    parser.add_argument("-db", "--database", type=str, required=True,
                        help='Chose the database: Dresden or Socrates')
    parser.add_argument("-l", "--level", type=str, required=True,
                        help='Chose the level of difficulty')
    parser.add_argument("-mt", "--montecarlo", type=int, required=True, 
                        help='Chose the number of test for Monte-Carlo')
    args = parser.parse_args()
    
    ## Training ##
    if args.mode == "extractor":
        ## Datasets ##
        train = pd.read_csv(path+args.database+'/train_img.csv', index_col=0)
        val = pd.read_csv(path+args.database+'/val_img.csv', index_col=0)
        train_gen = generator.DataGenerator(dataset=train, image_path=path, label=args.level, img_width=128, img_height=128)
        val_gen = generator.DataGenerator(dataset=val, image_path=path, label=args.level, img_width=128, img_height=128)
        ## Model ##
        model = MILSNet128(img_width=128, img_height=128, cam = len(np.unique(val[args.level].values)))
        # Training
        train(model, path+'Model/'+args.database+'_device.hdf5', train_gen, val_gen)
    elif args.mode == "train":
        ## Datasets ##
        train = dataset.pairs_patchs_train(dataset=pd.read_csv(path+args.database+'/train_img.csv', index_col=0))
        val = dataset.pairs_patchs_train(dataset=pd.read_csv(path+args.database+ '/validation_img.csv', index_col=0))
        ## Model ##
        model = Similarity_128(model_path=path + 'Model/'+args.database+"_mayer_CNN.hdf5", img_width=128, img_height=128)
        ## Generator ##
        train_gen = generator.SiameseGenerator(dataset=train, image_path=path, img_width=128, img_height=128)
        val_gen = generator.SiameseGenerator(dataset=val, image_path=path, img_width=128, img_height=128)
        # Training
        train(model, path+'Model/'+args.database+'_mayer_device.hdf5', train_gen, val_gen)
    ## Testing ##
    else:
        ## Datasets ##
        test = pd.read_csv(path+args.database+'/test_img.csv', index_col=0)
        # Choose the right path to the model
        model_path = path + 'Model/'+args.database +'_mayer_device.hdf5'
        ## Load the model
        mayer = load_json_model(model_path)
        acc = [0, 0, 0]
        auc = [0, 0, 0]
        tmp = [0, 0, 0]
        print("Data ready")
        for j in range(args.montecarlo):
            test_gen_1 = generator.SiameseGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_brand"), image_path=path, img_width=128, img_height=128) # , batch_size=len(test)//10) 
            test_gen_2 = generator.SiameseGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_model"), image_path=path, img_width=128, img_height=128)
            test_gen_3 = generator.SiameseGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_device"), image_path=path, img_width=128, img_height=128)
            tmp[0] = evaluation(model=mayer, test=test_gen_1)
            tmp[1] = evaluation(model=mayer, test=test_gen_2)
            tmp[2] = evaluation(model=mayer, test=test_gen_3)
            for i in range(len(acc)):
                acc[i] += tmp[i][0]
                auc[i] += tmp[i][1]
        for i in range(len(acc)):
            print("Accuracy of", acc[i]/args.montecarlo)
            print("Area under Curve", auc[i]/args.montecarlo)
if __name__ == "__main__":
    main()