import tensorflow.keras as keras 
import warnings, generator, dataset, argparse, os
import numpy as np
import pandas as pd
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras.backend import bias_add, variable, reshape
from tensorflow.keras.models import Model, load_model, Sequential
from tensorflow.keras.initializers import *
from scipy.signal import convolve2d
from sklearn import metrics
from scipy.spatial import distance
warnings.filterwarnings('ignore')


def train(model, name, train, val, epochs = 30):
    ## Callbacks ##
    MC = ModelCheckpoint(name, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    ES = EarlyStopping(monitor='val_acc', min_delta=0.01, patience = 2, verbose = 1, restore_best_weights=True)
    RL = ReduceLROnPlateau(monitor="val_acc", mode="max", factor=0.1, patience=2, verbose=1)
    CL =  ConstrainLayer()
    ## Train ##
    model.fit(train,
                        steps_per_epoch = train.__len__(),
                        epochs = epochs,
                        verbose = 1,
                        callbacks= [RL, MC, CL],
                        validation_data = val,
                        validation_steps = val.__len__())

def normalized_ED(x, y):
    dist = []
    for index in range(len(x)):
        dist.append(1/(1+distance.euclidean(x[index], y[index])))
    return np.array(dist) > sum(dist)/len(dist)

def evaluation(model, test, MFR=0):
    acc=0
    auc=0
    ## Generator ##
    X, y = test.__getitem__(0)
    if MFR == 0:
        pred1 = model.predict(X[:,0])
        pred2 = model.predict(X[:,1])
        pred = normalized_ED(pred1, pred2)
    else:
        pred1 = model.predict(X[0])
        pred2 = model.predict(X[1])    
        pred = normalized_ED(pred1, pred2)
    acc += metrics.accuracy_score(y, pred)
    auc += metrics.roc_auc_score(y, pred)
    return (acc*100), auc

def load_json_model(path, nb_classes=0):
    model = load_model(path)
    new_model = Model(inputs=model.inputs, outputs=model.layers[-2].output)
    new_model.set_weights(model.get_weights()[:-2])
    print("Loaded model from disk")
    if nb_classes !=0:
        x = model.layers[-2].output
        prediction = Dense(nb_classes, activation='softmax', name ='final')(x)
        model = Model(inputs=model.inputs, outputs=prediction)
        sgd = SGD(lr=0.001, momentum=0.95, decay=0.0005)
        model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['acc'])
    return new_model

class ConstrainLayer(keras.callbacks.Callback):

    def __init__(self):
        super(ConstrainLayer, self).__init__()
    # Utilized before each batch
    def on_batch_begin(self, batch, logs={}):
        # Get the weights of the first layer
        all_weights = self.model.get_weights()
        weights = np.asarray(all_weights[0])
        # Constrain the first layer
        weights = self.constrainLayer(weights)
        # Return the constrained weights back to the network
        all_weights[0] = weights
        self.model.set_weights(all_weights)

    def constrainLayer(self, weights):
        # Set central values to zero to exlude them from the normalization step
        weights[2,2,:,:]=0

        # Pass the weights
        filter_1 = weights[:,:,0,0]
        filter_2 = weights[:,:,0,1]
        filter_3 = weights[:,:,0,2]

        # Normalize the weights for each filter.
        # Sum in the 3rd dimension, which contains 25 numbers.
        filter_1 = filter_1.reshape(1,1,1,25)
        filter_1 = filter_1/filter_1.sum(3).reshape(1,1,1,1)
        filter_1[0,0,0,12] = -1

        filter_2 = filter_2.reshape(1,1,1,25)
        filter_2 = filter_2/filter_2.sum(3).reshape(1,1,1,1)
        filter_2[0,0,0,12] = -1

        filter_3 = filter_3.reshape(1,1,1,25)
        filter_3 = filter_3/filter_3.sum(3).reshape(1,1,1,1)
        filter_3[0,0,0,12] = -1

        # Prints are for debug reasons.
        # The sums of all filter weights for a specific filter
        # should be very close to zero.
        '''
        print(filter_1)
        print(filter_2)
        print(filter_3)
        '''
        '''
        print(filter_1.sum(3).reshape(1,1,1,1))
        print(filter_2.sum(3).reshape(1,1,1,1))
        print(filter_3.sum(3).reshape(1,1,1,1))
        '''
        # Reshape to original size.
        filter_1 = filter_1.reshape(1,1,5,5)
        filter_2 = filter_2.reshape(1,1,5,5)
        filter_3 = filter_3.reshape(1,1,5,5)

        # Pass the weights back to the original matrix and return.
        weights[:,:,0,0] = filter_1
        weights[:,:,0,1] = filter_2
        weights[:,:,0,2] = filter_3
        return weights

def MILS128(img_width, img_height, cam):
    inputs = Input(shape=(img_width, img_height,1))
    x = Conv2D(3, (5,5), strides=1)(inputs)              # Constrained Conv
                                                         # No activation is performed on this layer,
                                                         # like it is described in the paper.
    x = Conv2D(96, (7,7), strides=2,padding='same')(x)   # Conv 2
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(x)

    x = Conv2D(64, (5,5), strides=1, padding='same')(x)  # Conv 3
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2,)(x)

    x = Conv2D(64, (5,5), strides=1, padding='same')(x)   # Conv 4
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2,)(x)

    x = Conv2D(128, (1,1), strides=1, padding='same')(x)  # Conv 5
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = AveragePooling2D(pool_size=(3,3), strides=2,)(x)

    x = Flatten()(x)

    x = Dense(200)(x)                            # Fully Connected Layer 1
    x = Activation('tanh')(x)
    x = Dense(200)(x)                            # Fully Connected Layer 2
    x = Activation('tanh')(x)
    outputs = Dense(cam, activation='softmax')(x)        # Fully Connected Layer 3

    model = Model(inputs,outputs)
    model.summary()
    sgd = SGD(lr=0.001, momentum=0.95, decay=0.0005)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['acc'])
    return model

def MILSMFR(img_width, img_height, cam):
    input1 = Input(shape=(img_width, img_height,1))
    input2 = Input(shape=(img_width, img_height,1))
    print(input1)
    print(input2)
    x1 = Conv2D(3, (5,5), strides=1, padding='same')(input1)              # Constrained Conv
                                                         # No activation is performed on this layer,
                                                         # like it is described in the paper.
    x = Concatenate()([x1, input2])

    x = Conv2D(96, (7,7), strides=2,padding='same')(x)   # Conv 2
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2, padding='same')(x)

    x = Conv2D(64, (5,5), strides=1, padding='same')(x)  # Conv 3
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2,)(x)

    x = Conv2D(64, (5,5), strides=1, padding='same')(x)   # Conv 4
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = MaxPooling2D(pool_size=(3,3), strides=2,)(x)

    x = Conv2D(128, (1,1), strides=1, padding='same')(x)  # Conv 5
    x = BatchNormalization()(x)
    x = Activation('tanh')(x)
    x = AveragePooling2D(pool_size=(3,3), strides=2,)(x)

    x = Flatten()(x)

    x = Dense(200)(x)                            # Fully Connected Layer 1
    x = Activation('tanh')(x)
    x = Dense(200)(x)                            # Fully Connected Layer 2
    x = Activation('tanh')(x)
    outputs = Dense(cam, activation='softmax')(x)        # Fully Connected Layer 3

    model = Model([input1, input2],outputs)
    model.summary()
    sgd = SGD(lr=0.001, momentum=0.95, decay=0.0005)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['acc'])
    return model


def main():
    ## Variables ##
    path = "//medias/db/ImagingSecurity_misc/Berthet/Camera_identification/"
    ## Args ##
    parser = argparse.ArgumentParser(description='Chose the parameters for training or testing.')
    parser.add_argument("-m", "--mode", type=str, required=True,
                        help='Chose the mode you want: train - test')
    parser.add_argument("-db", "--database", type=str, required=True,
                        help='Chose the database: Dresden or Socrates')
    parser.add_argument("-l", "--level", type=str, required=True,
                        help='Chose the level of difficulty')
    parser.add_argument("-MFR", type=int, required=True, 
                        help='Chose method with MFR or not')
    parser.add_argument("-mt", "--montecarlo", type=int, required=True, 
                        help='Chose the number of test for Monte-Carlo')
    args = parser.parse_args()
    
    ## Training ##
    if args.mode == "train":
        ## Datasets ##
        train = pd.read_csv(path+args.database+'/train_img.csv', index_col=0)
        val = pd.read_csv(path+args.database+ '/validation_img.csv', index_col=0)
        ## Model ##
        if args.MFR==1:
            args.database += '_bayar_MFR'
            model = MILSMFR(img_width=128, img_height=128, cam=len(np.unique(val[args.level].values)))
        else:
            args.database += '_bayar'
            model = MILS128(img_width=128, img_height=128, cam=len(np.unique(val[args.level].values)))
        ## Generator ##
        train_gen = generator.BayarGenerator(dataset=train, image_path=path, img_width=128, img_height=128, MFR=args.MFR, level=args.level) 
        val_gen = generator.BayarGenerator(dataset=val, image_path=path, img_width=128, img_height=128, MFR=args.MFR, level=args.level) 
        # Training
        train(model, path+'Model/'+args.database+'_device.hdf5', train_gen, val_gen)
    ## Testing ##
    else:
        ## Datasets ##
        test = pd.read_csv(path+args.database+'/test_img.csv', index_col=0)
        # Choose the right path to the model
        if args.MFR==1:
            model_path = path + 'Model/' + args.database + '_bayar_MFR_device.hdf5'
        else:
            model_path = path + 'Model/' + args.database + '_bayar_device.hdf5'
        ## Load the model
        bayar = load_json_model(path=model_path)
        ## N test to Monte-Carlo
        acc = [0, 0, 0]
        auc = [0, 0, 0]
        tmp = [0, 0, 0]
        print("Data ready")
        for i in range(args.montecarlo):
            test_gen_1 = generator.BayarGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_brand"), image_path=path, img_width=128, img_height=128, MFR=args.MFR)
            test_gen_2 = generator.BayarGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_model"), image_path=path, img_width=128, img_height=128, MFR=args.MFR)
            test_gen_3 = generator.BayarGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_device"), image_path=path, img_width=128, img_height=128, MFR=args.MFR)
            tmp[0] = evaluation(model=bayar, test=test_gen_1, MFR=args.MFR)
            tmp[1] = evaluation(model=bayar, test=test_gen_2, MFR=args.MFR)
            tmp[2] = evaluation(model=bayar, test=test_gen_3, MFR=args.MFR)
            for i in range(len(acc)):
                acc[i] += tmp[i][0]
                auc[i] += tmp[i][1]
        for i in range(len(acc)):
            print("Accuracy of", acc[i]/args.montecarlo)
            print("Area under Curve", auc[i]/args.montecarlo)

if __name__ == "__main__":
    main()