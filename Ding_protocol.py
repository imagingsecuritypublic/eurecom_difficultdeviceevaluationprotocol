import warnings, generator, argparse, dataset, os
import numpy as np
import pandas as pd
import tensorflow.keras
from tensorflow.keras.layers import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.initializers import *
from sklearn import metrics
from scipy.spatial import distance
import keras.backend as K
warnings.filterwarnings('ignore')
os.environ["CUDA_VISIBLE_DEVICES"]="0, 1"


def train(model, name, train, val, epochs = 30):
    ## Callbacks ##
    MC = ModelCheckpoint(name, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    ES = EarlyStopping(monitor='val_acc', min_delta=0.01, patience = 2, verbose = 1, restore_best_weights=True)
    RL = ReduceLROnPlateau(monitor="val_acc", mode="max", factor=0.1, patience=2, verbose=1)
    CL =  ConstrainLayer()
    ## Train ##
    model.fit(train,
                        steps_per_epoch = train.__len__(),
                        epochs = epochs,
                        verbose = 1,
                        callbacks= [RL, MC, CL],
                        validation_data = val,
                        validation_steps = val.__len__())


def normalized_ED(x, y):
    dist = []
    for index in range(len(x)):
        dist.append(1/(1+distance.euclidean(x[index], y[index])))
    return np.array(dist) > sum(dist)/len(dist)

def evaluation(model, test):
    acc=0
    auc=0
    ## Generator ##
    X, y = test.__getitem__(0)
    pred1 = model.predict(X[0])
    pred2 = model.predict(X[1])
    pred = normalized_ED(pred1, pred2)
    acc += metrics.accuracy_score(y, pred)
    auc += metrics.roc_auc_score(y, pred)
    return acc, auc

def load_json_model(path, nb_classes=0):
    model = load_model(path)
    new_model = Model(inputs=model.inputs, outputs=model.layers[-2].output)
    new_model.set_weights(model.get_weights()[:-2])
    print("Loaded model from disk")
    if nb_classes != 0:
        x = model.layers[-2].output
        prediction = Dense(nb_classes, activation='softmax', name ='final')(x)
        model = Model(inputs=model.inputs, outputs=prediction)
        sgd = SGD(lr= 0.005, momentum = 0.95, decay=0.0005)
        model.compile(loss="binary_crossentropy", optimizer=sgd, metrics=['acc'])
    return new_model

def conv(inputs, w, k, s=1, p='same'):
    x = Conv2D(w, kernel_size = k, strides=s, padding= p)(inputs)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    return x

def ResNet(inputs, filters, stride):
    x1 = conv(inputs, filters, 3, stride)
    x1 = conv(x1, filters, 3)
    x2 = conv(inputs, filters, 1, stride)
    x = Add()([x1, x2])
    x = Activation('relu')(x)
    return x

def ResNetx3(inputs, filters, stride):
    x = ResNet(inputs, filters, stride)
    x = ResNet(inputs, filters, 1)
    x = ResNet(inputs, filters, 1)
    return x

def prepro(x, n):
    x = conv(x, 16, 7, 2)
    x = ResNetx3(x, 16, 1)
    return x

def MultiHPF(x1, x2, x3, x4):
    x1 = prepro(x1,'x1')
    x2 = prepro(x2,'x2')
    x3 = prepro(x3,'x3')
    x4 = prepro(x4,'x4')
    x = Concatenate(axis=3)([x1, x2, x3, x4])
    return x

def network(img_width, img_height, cam):
    ## Inputs ##
    inputs = Input((img_width, img_height,3))
    inputN1 = Input((img_width, img_height,3))
    inputN2 = Input((img_width, img_height,3))
    inputN3 = Input((img_width, img_height,3))
    ## Pre Processing ##
    x = MultiHPF(inputs, inputN1, inputN2, inputN3)
    ## ResNet Modules ##
    x = ResNetx3(x, 128, 2)
    x = ResNetx3(x, 256, 2)
    x = ResNetx3(x, 512, 2)
    x = GlobalAveragePooling2D()(x)
    outputs = Dense(cam, activation='softmax')(x)
    model = Model([inputs, inputN1, inputN2, inputN3], outputs)
    model.summary()
    sgd = SGD(lr=0.1, momentum=0.9, decay=0.00001)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['acc'])
    return model


def main():
    ## Variables ##
    path = "//medias/db/ImagingSecurity_misc/Berthet/Camera_identification/"
    ## Args ##
    parser = argparse.ArgumentParser(description='Chose the parameters for training or testing.')
    parser.add_argument("-m", "--mode", type=str, required=True,
                        help='Chose the mode you want: train - test')
    parser.add_argument("-db", "--database", type=str, required=True,
                        help='Chose the database: Dresden or Socrates')
    parser.add_argument("-l", "--level", type=str, required=True,
                        help='Chose the level of difficulty')
    parser.add_argument("-mt", "--montecarlo", type=int, required=True, 
                        help='Chose the number of test for Monte-Carlo')
    args = parser.parse_args()
    
    ## Training ##
    if args.mode == "train":
        ## Datasets ##
        train = pd.read_csv(path+args.database+'/train_img.csv', index_col=0)
        val = pd.read_csv(path+args.database+ '/validation_img.csv', index_col=0)
        ## Model ##
        model = network(img_width=128, img_height=128, cam=len(np.unique(val.label_device.values)))
        ## Generator ##
        train_gen = generator.DingGenerator(dataset=train, image_path=path, img_width=128, img_height=128, level=args.level)
        val_gen = generator.DingGenerator(dataset=val, image_path=path, img_width=128, img_height=128, level=args.level)
        # Training
        train(model, path+'Model/'+args.database+'_ding_device.hdf5', train_gen, val_gen)
    ## Testing ##
    else:
        ## Datasets ##
        test = pd.read_csv(path+args.database+'/test_img.csv', index_col=0)
        # Choose the right path to the model
        model_path = path + 'Model/' + args.database +'_ding_device.hdf5'
        ## Load the model
        ding = load_json_model(path=model_path)
        ## N test to Monte-Carlo
        acc = [0, 0, 0]
        auc = [0, 0, 0]
        tmp = [0, 0, 0]
        print("Data ready")
        for i in range(args.montecarlo):
            test_gen_1 = generator.DingGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_brand"), image_path=path, img_width=128, img_height=128)
            test_gen_2 = generator.DingGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_model"), image_path=path, img_width=128, img_height=128)
            test_gen_3 = generator.DingGenerator(dataset=dataset.pairs_patchs(dataset=test, level="label_device"), image_path=path, img_width=128, img_height=128)
            tmp[0] = evaluation(model=ding, test=test_gen_1)
            tmp[1] = evaluation(model=ding, test=test_gen_2)
            tmp[2] = evaluation(model=ding, test=test_gen_3)
            for i in range(len(acc)):
                acc[i] += tmp[i][0]
                auc[i] += tmp[i][1]
        for i in range(len(acc)):
            print("Accuracy of", acc[i]/args.montecarlo)
            print("Area under Curve", auc[i]/args.montecarlo)
if __name__ == "__main__":
    main()