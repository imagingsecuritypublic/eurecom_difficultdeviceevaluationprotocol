import sys, cv2, os
import numpy as np
import pandas as pd

def image_to_patch(img,size):
    L = int(np.floor(img.shape[0]/size))
    M = int(np.floor(img.shape[1]/size))
    patches = np.ones((L*M,size,size,3),dtype=np.uint8)
    for i in range(L):
        for j in range(M):
            patches[i*M+j]=img[i*size:(i+1)*size,j*size:(j+1)*size,:]
    return patches

def dataset_patches(dataset, ori_path, fin_path, folder, size):
    image_data = []
    models_data = []
    devices_data =[]
    brands_data = []
    nb_img = 0
    dataset = dataset[['img','label_devices','label_models','label_brands']].values
    for id_img, device, model, brand in dataset:
        if nb_img%100==0:
            print(nb_img)
        file_path = ori_path+id_img
        try:
            os.path.isfile(file_path) 
        except FileNotFoundError:
            print(file_path)
        else:        
            img = cv2.imread(file_path)
            hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
            bright = hsv[:,:,2].mean()
            patches = image_to_patch(img,size)
            nb= 0
            for k in range(patches.shape[0]):
                tmp = patches[k]
                hsv_tmp = cv2.cvtColor(tmp, cv2.COLOR_BGR2HSV)
                hsv_tmp = hsv_tmp[:,:,2].mean()
                if ((hsv_tmp > bright*1.2) & (hsv_tmp < 240)):
                    image_data.append(folder + id_img[:-4]+"_"+str(nb)+".jpg")
                    models_data.append(model)
                    devices_data.append(device)
                    brands_data.append(brand)
                    cv2.imwrite(fin_path+folder+ id_img[:-4]+"_"+str(nb)+".jpg", tmp)
                    nb+=1
            nb_img +=1
    print("Done")
    dataset = pd.DataFrame(np.transpose(np.stack([image_data, devices_data, models_data, brands_data])), columns=['img','label_device','label_model','label_brand'])
    return dataset

def pairs_patchs_train(dataset):
    image_data1 = []
    image_data2 = []
    label_binary = []
    tmp = dataset.sample(frac=0.5)
    for img, device in tmp[["img", "label_device"]].values:
        tmp2 = dataset[dataset.label_device != device]
        if len(tmp2) > 10:
            ## Selection of random patches from previous dataframe
            tmp2 = tmp2.sample(10)
            ## Storing values
            image_data1.extend(10*[img])
            image_data2.extend(tmp2.img.values)
            label_binary.extend(10*[0])
            ## Selection of random patches of same camera 
            tmp3 = dataset[dataset.label_device == device]
            if len(tmp3)>10:
                tmp3 = tmp3.sample(10)
                ## Storing values
                image_data1.extend(10*[img])
                image_data2.extend(tmp3.img.values)
                label_binary.extend(10*[1])
    final_df = pd.DataFrame(np.transpose(np.stack([image_data1, image_data2])),columns=['img1','img2'])
    final_df['label'] = label_binary
    #final_df.to_csv(path)
    return final_df

def pairs_patchs(dataset, level):
    image_data1 = []
    image_data2 = []
    label_binary = []
    label1 = []
    label2 = []
    ## Selection of all different label for the level
    list_label = np.unique(dataset[level].values)
    nb_patch = np.min([dataset.groupby(level)[level].value_counts().min(), 1000])
    ## Creation of a distributed dataframe according to the number of different label
    tmp = pd.DataFrame()
    for label in list_label:
        tmp = tmp.append(dataset[dataset[level]==label].sample(nb_patch))
    for img, label, brand, model, device in tmp[["img", level, "label_brand", "label_model", "label_device"]].values:
        ## Selection of patches of different cameras
        if level == "label_brand":
            ## First level: not sharing same brand label
            tmp2 = tmp[tmp.label_brand != label]
        elif level == "label_model":
            ## Second level: sharing brand label 
            tmp2 = tmp[tmp.label_brand == brand]
            ## And different model label
            tmp2 = tmp2[tmp2.label_model != label]
        elif level == "label_device":
            ## Third level: sharing camera label
            tmp2 = dataset[dataset.label_model == model]
            ## And different device label
            tmp2 = tmp2[tmp2.label_device != label]
        if len(tmp2) > 10:
            ## Selection of random patches from previous dataframe
            tmp2 = tmp2.sample(10)
            ## Storing values
            image_data1.extend(10*[img])
            label1.extend(10*[label])
            image_data2.extend(tmp2.img.values)
            label2.extend(tmp2[level].values)
            label_binary.extend(10*[0])
            ## Selection of random patches of same camera 
            tmp3 = dataset[dataset.label_device == device]
            if len(tmp3)>10:
                tmp3 = tmp3.sample(10)
                ## Storing values
                image_data1.extend(10*[img])
                label1.extend(10*[label])
                image_data2.extend(tmp3.img.values)
                label2.extend(tmp3[level].values)
                label_binary.extend(10*[1])
    final_df = pd.DataFrame(np.transpose(np.stack([image_data1, image_data2])),columns=['img1','img2'])
    final_df['label'] = label_binary
    final_df['label_img1'] = label1
    final_df['label_img2'] = label2
    #print("Done for "+level)
    #final_df.to_csv(path)
    return final_df

def main():
    ## Path ##
    db = sys.argv[1]
    choice = sys.argv[2]
    path = "//medias/db/ImagingSecurity_misc/Berthet/Camera_identification/"
    
    ## Database and path to images
    if db == 'Dresden':
        path2 = '//medias/db/forensics/Dresden/Dresden/'
        df = pd.read_csv(path + db + "/dresden_cameras.csv",index_col=0)
    elif db == 'Socrates':
        path2 = '//medias/db/forensics/SOCRatES/Images/'
        df = pd.read_csv(path + db + "/df_socrates.csv",index_col=0)
        
    ## Split database in training / validation / testing
    if choice== 'split':
        train_img, validation_img, test_img = np.split(df.sample(frac=1), [int(.6*len(df)), int(.8*len(df))]) # Choose the ratio (60:20:20)
        # Save the dataframes in the path you want
        train_img.to_csv(path+db+'/train.csv')
        validation_img.to_csv(path+db+'/validation.csv')
        test_img.to_csv(path+db+'/test.csv')
        print("Done")
    ## Create patches from the previous dataframes 
    elif choice == 'patch':
        # Upload the dataframes
        train_img = pd.read_csv(path+db+'/train.csv',index_col=0)
        validation_img = pd.read_csv(path+db+'/validation.csv',index_col=0)
        test_img = pd.read_csv(path+db+'/test.csv',index_col=0)
        # Create and store the patches in the path you want
        # Change fin_path and folder if you want to change the location
        train = dataset_patches(dataset=train_img, ori_path=path2, fin_path=path, folder= db+"/Training/",size=128)
        validation = dataset_patches(dataset=validation_img, ori_path=path2, fin_path=path, folder= db+"/Validation/",size=128)
        test = dataset_patches(dataset=test_img, ori_path=path2, fin_path=path, folder= db+"/Testing/",size=128)
        # Store the dataframe of these patches
        train.to_csv(path+db+'/train_img.csv')
        validation.to_csv(path+db+'/validation_img.csv')
        test.to_csv(path+db+'/test_img.csv')
    ## Create the dataset of pairs
    elif choice == "pair":
        # Upload the dataframe
        test = pd.read_csv(path+db+'/test_img.csv')
        pairs_patchs(dataset=test, path=path+db+'/pairs_level1.csv', level='label_brand')
        pairs_patchs(dataset=test, path=path+db+'/pairs_level2.csv', level='label_model')
        pairs_patchs(dataset=test, path=path+db+'/pairs_level3.csv', level='label_device')

if __name__ == "__main__":
    main()